/** @type {import('next').NextConfig} */
const nextConfig = {
  output: 'standalone',
  distDir: 'build',
  basePath: '/tekton-app',
}

module.exports = nextConfig
